---
date: 2017-02-09T00:11:02+01:00
title: Tools Used 
weight: 30
---

## Tools used in the environment?

The DevSecOps Studio Project uses the following tools.

![](/images/devsecops-tools.png)

## Detailed tool list

 Technology  | Tools
------------ | -------------
PenTest Toolkit: | Nmap, Metasploit
Static Analysis Tools: | Brakeman, bandit, FindSecBugs
Dynamic Analysis Tools: | ZAP proxy, Gaunlt
Hardening: | DevSec Ansible OS Hardening
Compliance: | Inspec
Operating System :| Ubuntu Xenial (16.04)
Programming Languages: | Java, Python 2, Python 3, Ruby/Rails
Container Technology:| Docker
Source Code Management:| Gitlab (Github like system)
CI Server:| Gitlab CI/Jenkins
Configuration Management:| Ansible
Monitoring and Log management:| Elastic Search, LogStash and Kibana
Cloud Provider Utilities:| AWS CLI
Utilities:| Git, Vim, Curl, Wget,
